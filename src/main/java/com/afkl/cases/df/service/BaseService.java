package com.afkl.cases.df.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class BaseService {

	@Autowired
	protected ObjectMapper mapper;

	@Autowired
	protected OAuth2RestTemplate restTemplate;
	
	public void generateRestTemplate(OAuth2RestTemplate restTemplate) {
		if((this.restTemplate == null || this.restTemplate.getAccessToken() == null) && restTemplate != null && restTemplate.getAccessToken() != null) {
			this.restTemplate = restTemplate;
		}
		
		if(mapper == null || mapper.getSerializationConfig() == null)
			mapper = new ObjectMapper();
	}

}
