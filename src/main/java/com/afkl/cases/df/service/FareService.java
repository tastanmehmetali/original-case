package com.afkl.cases.df.service;

import java.io.IOException;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.afkl.cases.df.domain.FareInfo;
import com.afkl.cases.df.domain.Location;
import com.afkl.cases.df.domain.dto.FareInfoResultDto;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
public class FareService extends BaseService implements IFareService {

	private static final Logger logger = LoggerFactory.getLogger(FareService.class);

	@Value("${api.endpoint.base}")
	private String baseUrl;

	@Value("${api.endpoint.fare}")
	private String fareUrl;

	@Value("${api.endpoint.timeout}")
	private long timeout;

	@Async
	@Override
	public Future<FareInfoResultDto> findFare(String origin, String destination) throws JsonParseException, JsonMappingException, IOException {
		if (StringUtils.isEmpty(origin) || StringUtils.isEmpty(destination))
			return null;

		String url = String.format(fareUrl, baseUrl, origin, destination);
		logger.info("using url: {}, to find Fares by orgin with destination", url);

		String json = this.restTemplate.getForObject(url, String.class);
		logger.info("received json: {}", json);

		FareInfoResultDto fareInfoResultDto = mapper.readValue(json, FareInfoResultDto.class);
		logger.info("fareInfoResultDto: {}", fareInfoResultDto.toString());

		return new AsyncResult<>(fareInfoResultDto);
	}

	@Async
	@Override
	public FareInfo genarateFareInfo(FareInfoResultDto fareInfoResultDto, Location orginLocation,
			Location destinationLocation) {
		FareInfo fareInfo = new FareInfo();
		fareInfo.setAmount(fareInfoResultDto.getAmount());
		fareInfo.setCurrency(fareInfoResultDto.getCurrency());
		fareInfo.setOrgin(orginLocation);
		fareInfo.setDestination(destinationLocation);

		return fareInfo;
	}

	public void generatedValueFromApplicationProperties(String baseUrl, String fareUrl, long timeout) {
		if (this.baseUrl == null && this.fareUrl == null) {
			this.fareUrl = fareUrl;
			this.baseUrl = baseUrl;
			this.timeout = timeout;
		}
	}

}
