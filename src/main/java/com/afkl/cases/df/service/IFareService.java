package com.afkl.cases.df.service;

import java.io.IOException;
import java.util.concurrent.Future;

import com.afkl.cases.df.domain.FareInfo;
import com.afkl.cases.df.domain.Location;
import com.afkl.cases.df.domain.dto.FareInfoResultDto;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface IFareService {

	Future<FareInfoResultDto> findFare(String origin, String destination) throws JsonParseException, JsonMappingException, IOException;

	FareInfo genarateFareInfo(FareInfoResultDto fareInfoResultDto, Location orginLocation, Location destinationLocation);
}
