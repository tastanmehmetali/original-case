package com.afkl.cases.df.service;

import java.io.IOException;
import java.util.concurrent.Future;

import com.afkl.cases.df.domain.Location;
import com.afkl.cases.df.domain.dto.LocationResultDto;

public interface IAirportService {

	public Future<LocationResultDto> getAirports(String givenSearch) throws IOException;

	public Future<Location> findAirportByGivenCode(String givenCode);
	
}
