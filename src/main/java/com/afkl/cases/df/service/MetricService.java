package com.afkl.cases.df.service;

import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.MetricsEndpoint;
import org.springframework.stereotype.Service;

import com.afkl.cases.df.domain.Metrics;

@Service
public class MetricService implements IMetricService {

	private static final Logger logger = LoggerFactory.getLogger(MetricService.class);

	@Autowired
	private MetricsEndpoint metricsEndpoint;

	@Override
	public Metrics getMetrics() {

		Metrics metrics = new Metrics();
		if(metricsEndpoint == null || metricsEndpoint.listNames() == null) 
			return null;
		
		Set<String> metricSet = metricsEndpoint.listNames().getNames();

		metrics.setRequestCount(sum(metricSet, "counter.status."));
		metrics.setRequestCountOK(sum(metricSet, "counter.status.2"));
		metrics.setRequestCount4xx(sum(metricSet, "counter.status.4"));
		metrics.setRequestCount5xx(sum(metricSet, "counter.status.5"));
		metrics.setMinResponseTime(get(metricSet, "gauge.response.min"));
		metrics.setMaxResponseTime(get(metricSet, "gauge.response.max"));
		metrics.setAvgResponseTime(get(metricSet, "gauge.response.total"));
		if (metrics.getRequestCount() > 0) {
			metrics.setAvgResponseTime(metrics.getAvgResponseTime() / metrics.getRequestCount());
		}

		return metrics;
	}

	private int sum(Set<String> metricSet, String prefix) {
		return metricSet.stream().filter(e -> e.startsWith(prefix)).mapToInt(value -> Integer.parseInt(value)).sum();
	}

	private long get(Set<String> metricSet, String prefix) {
		Optional<String> listOfString = metricSet.stream().filter(e -> e.startsWith(prefix)).findFirst();
		long returnVal = 0;
		if (listOfString.isPresent()) {
			returnVal = calcforGivenNum(listOfString.get());
		}
		logger.info("metrics [get method] returnVal: {}", returnVal);
		return returnVal;
	}

	private long calcforGivenNum(String entry) {
		return (long) Double.parseDouble(entry);
	}

}
