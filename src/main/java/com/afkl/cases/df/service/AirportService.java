package com.afkl.cases.df.service;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.afkl.cases.df.domain.Location;
import com.afkl.cases.df.domain.dto.LocationResultDto;
import com.afkl.cases.df.domain.dto.SearchResultDto;

@Service
public class AirportService extends BaseService implements IAirportService {

	private static final Logger logger = LoggerFactory.getLogger(AirportService.class);

	@Value("${api.endpoint.base}")
	private String baseUrl;

	@Value("${api.endpoint.airport}")
	private String airportUrl;

	@Value("${api.endpoint.airport.search}")
	private String airportUrlForSearch;

	@Async
	@Override
	public Future<LocationResultDto> getAirports(String givenSearch) throws IOException {
		String url = generateAirportTemplate(givenSearch);
		logger.info("using url: {}, to find Airports by givenSearch: {}", url, givenSearch);
		String json = restTemplate.getForObject(url, String.class);

		logger.debug("received json: {}", json);
		SearchResultDto response = mapper.readValue(json, SearchResultDto.class);

		return new AsyncResult<>(response.getLocationResultDto());
	}

	private String generateAirportTemplate(String givenText) {
		String url = String.format(airportUrlForSearch, baseUrl, givenText);
		if(StringUtils.isBlank(givenText)) {
			url = String.format(airportUrl, baseUrl);
		}
		
		return url;
	}

	public void generatedValueFromApplicationProperties(String baseUrl, String airportUrl, String airportUrlForSearch) {
		if (this.airportUrl == null && this.baseUrl == null && this.airportUrlForSearch == null) {
			this.airportUrl = airportUrl;
			this.baseUrl = baseUrl;
			this.airportUrlForSearch = airportUrlForSearch;
		}
	}
	
	@Async
	@Override
	public Future<Location> findAirportByGivenCode(String givenCode) {
		if(StringUtils.isBlank(givenCode))
			return null;
		
		String url = generateAirportTemplate(givenCode);
		logger.info("using url: {}, to find Airports by givenCode: {}", url, givenCode);
		
		String json = restTemplate.getForObject(url, String.class);

		logger.info("[code] received json: {}", json);
		SearchResultDto searchResultDto = restTemplate.getForObject(url, SearchResultDto.class);
        Optional<Location> locationOption = searchResultDto.getLocationResultDto().getLocations().stream().findFirst();
        Location location = null;
        if(locationOption.isPresent())
        	location = locationOption.get();
        return new AsyncResult<>(location);
	}

}
