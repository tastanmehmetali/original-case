package com.afkl.cases.df.service;

import com.afkl.cases.df.domain.Metrics;

public interface IMetricService {

	Metrics getMetrics();

}
