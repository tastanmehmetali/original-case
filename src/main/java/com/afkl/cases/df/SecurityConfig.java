package com.afkl.cases.df;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${api.endpoint.authUrl}")
    private String authUrl;

    @Value("${api.endpoint.clientId}")
    private String clientId;

    @Value("${api.endpoint.clientSecret}")
    private String secret;
    
    @Value("${api.endpoint.grantType}")
    private String grantType;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().permitAll();
    }
    
    @Bean
    public OAuth2RestTemplate restTemplate() {
        ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
        resourceDetails.setAccessTokenUri(authUrl);
        resourceDetails.setClientId(clientId);
        resourceDetails.setClientSecret(secret);
        resourceDetails.setGrantType(grantType);
        resourceDetails.setScope(Arrays.asList("read", "write"));

        return new OAuth2RestTemplate(resourceDetails, new DefaultOAuth2ClientContext());
    }

}
