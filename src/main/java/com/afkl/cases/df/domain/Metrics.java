package com.afkl.cases.df.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
@JsonIgnoreProperties(ignoreUnknown = true)
public class Metrics {

	private int requestCount;
	private int requestCountOK;
	private int requestCount4xx;
	private int requestCount5xx;
	private long avgResponseTime;
	private long minResponseTime;
	private long maxResponseTime;

	public int getRequestCount() {
		return requestCount;
	}

	public void setRequestCount(int requestCount) {
		this.requestCount = requestCount;
	}

	public int getRequestCountOK() {
		return requestCountOK;
	}

	public void setRequestCountOK(int requestCountOK) {
		this.requestCountOK = requestCountOK;
	}

	public int getRequestCount4xx() {
		return requestCount4xx;
	}

	public void setRequestCount4xx(int requestCount4xx) {
		this.requestCount4xx = requestCount4xx;
	}

	public int getRequestCount5xx() {
		return requestCount5xx;
	}

	public void setRequestCount5xx(int requestCount5xx) {
		this.requestCount5xx = requestCount5xx;
	}

	public long getAvgResponseTime() {
		return avgResponseTime;
	}

	public void setAvgResponseTime(long avgResponseTime) {
		this.avgResponseTime = avgResponseTime;
	}

	public long getMinResponseTime() {
		return minResponseTime;
	}

	public void setMinResponseTime(long minResponseTime) {
		this.minResponseTime = minResponseTime;
	}

	public long getMaxResponseTime() {
		return maxResponseTime;
	}

	public void setMaxResponseTime(long maxResponseTime) {
		this.maxResponseTime = maxResponseTime;
	}

}
