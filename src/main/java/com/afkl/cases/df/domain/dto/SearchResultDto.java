package com.afkl.cases.df.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResultDto {

	@JsonProperty("_embedded")
	private LocationResultDto locationResultDto;

	public LocationResultDto getLocationResultDto() {
		return locationResultDto;
	}
	
	public void setLocationResultDto(LocationResultDto locationResultDto) {
		this.locationResultDto = locationResultDto;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchResultDto{ _embedded='");
		builder.append(locationResultDto);
		builder.append("}");
		return builder.toString();
	}

}
