package com.afkl.cases.df.domain;

import com.afkl.cases.df.domain.util.Currency;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
@JsonIgnoreProperties(ignoreUnknown = true)
public class FareInfo {

	private double amount;
	private Currency currency;
	private Location orgin;
	private Location destination;
	
	public FareInfo() {
		currency = Currency.EUR;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency curreny) {
		if(curreny == null)
			curreny = Currency.EUR; 
		this.currency = curreny;
	}

	public Location getOrgin() {
		return orgin;
	}

	public void setOrgin(Location orgin) {
		this.orgin = orgin;
	}

	public Location getDestination() {
		return destination;
	}

	public void setDestination(Location destination) {
		this.destination = destination;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FareInfo { amount=");
		builder.append(amount);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", orgin=");
		builder.append(orgin);
		builder.append(", destination=");
		builder.append(destination);
		builder.append("}");
		return builder.toString();
	}

}
