package com.afkl.cases.df.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

@JsonSerializableSchema
@JsonIgnoreProperties(ignoreUnknown = true)
public class Location {

	private String code;
	private String name; 
	private String description;
	private Location parent;
	private Coordinate coordinates;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Location getParent() {
		return parent;
	}
	
	public void setParent(Location parent) {
		this.parent = parent;
	}

	public Coordinate getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Coordinate coordinates) {
		this.coordinates = coordinates;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Location{ code='");
		builder.append(code);
		builder.append("', name='");
		builder.append(name);
		builder.append("', description='");
		builder.append(description);
		builder.append("', parent=");
		builder.append(parent);
		builder.append("', coordinate=");
		builder.append(coordinates);
		builder.append("}");
		return builder.toString();
	}

}
