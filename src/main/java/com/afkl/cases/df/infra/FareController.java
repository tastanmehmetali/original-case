package com.afkl.cases.df.infra;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.domain.FareInfo;
import com.afkl.cases.df.domain.Location;
import com.afkl.cases.df.domain.dto.FareInfoResultDto;
import com.afkl.cases.df.service.IAirportService;
import com.afkl.cases.df.service.IFareService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@CrossOrigin
@RestController
@RequestMapping("/fares")
public class FareController {

	@Autowired
	private IFareService fareService;

	@Autowired
	private IAirportService airportService;

	@Value("${api.endpoint.timeout}")
	private long timeout;

	@GetMapping(value = "/{origin}/{destination}")
	public @ResponseBody FareInfo findFareInfo(@PathVariable("origin") String origin,
			@PathVariable("destination") String destination)
			throws InterruptedException, ExecutionException, TimeoutException, JsonParseException, JsonMappingException, IOException {

		if (StringUtils.isEmpty(origin) || StringUtils.isEmpty(destination))
			return null;

		Location orginLocation = generateLocationInfoFromAirportService(origin);
		if (orginLocation == null)
			return null;

		Location destinationLocation = generateLocationInfoFromAirportService(destination);
		if (destinationLocation == null)
			return null;

		Future<FareInfoResultDto> fareResponseFuture = fareService.findFare(origin, destination);
		FareInfoResultDto fareInfoResultDto = fareResponseFuture.get(timeout, TimeUnit.SECONDS);

		return fareService.genarateFareInfo(fareInfoResultDto, orginLocation, destinationLocation);
	}

	private Location generateLocationInfoFromAirportService(String givenCode)
			throws InterruptedException, ExecutionException, TimeoutException {
		Future<Location> locationFuture = airportService.findAirportByGivenCode(givenCode);
		if (locationFuture == null)
			return null;

		return locationFuture.get(timeout, TimeUnit.SECONDS);
	}
}
