package com.afkl.cases.df.infra;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.domain.Location;
import com.afkl.cases.df.domain.dto.LocationResultDto;
import com.afkl.cases.df.service.IAirportService;

@CrossOrigin
@RestController
@RequestMapping("/airports")
public class AirportController {

	@Value("${api.endpoint.timeout}")
	private long timeout;

	@Autowired
	private IAirportService airportService;

	@GetMapping
	public @ResponseBody List<Location> findAirport(
			@RequestParam(value = "search", required = false, defaultValue = "") String searchString)
			throws IOException, InterruptedException, ExecutionException, TimeoutException {

		Future<LocationResultDto> locationResponseFuture = airportService.getAirports(searchString);
		LocationResultDto locationSearchResult = locationResponseFuture.get(timeout, TimeUnit.SECONDS);

		if (locationSearchResult == null) {
			return new ArrayList<>();
		}

		return locationSearchResult.getLocations();
	}

}
