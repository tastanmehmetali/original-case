package com.afkl.cases.df.infra;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.afkl.cases.df.domain.Metrics;
import com.afkl.cases.df.service.IMetricService;

@CrossOrigin
@RestController
@RequestMapping("/metrics")
public class MetricsController {

	@Autowired
	private IMetricService metricsService;

	@GetMapping
	public @ResponseBody Metrics getMetrics() {
		return metricsService.getMetrics();
	}

}
