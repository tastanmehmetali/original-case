package com.afkl.cases.df.domain;

import org.junit.Assert;
import org.junit.Test;

public class CoordinateTest {

	private static final int ZERO = 0;

	@Test
	public void shouldBeCheckCoordinateWhenItIsNull() {
		Coordinate coordinate = null;
		Assert.assertNull(coordinate);
	}
	
	@Test
	public void shouldBeCheckCoordinateWhenItIsNewObject() {
		Coordinate coordinate = new Coordinate();
		Assert.assertNotNull(coordinate);
	}
	
	@Test
	public void shouldBeCheckCoordinateToString() {
		Coordinate coordinate = new Coordinate();
		Assert.assertEquals(generateToString(coordinate), coordinate.toString());
	}
	
	private String generateToString(Coordinate coordinate) {
		StringBuilder builder = new StringBuilder();
		builder.append("Coordinate{ latitude=");
		builder.append(coordinate.getLatitude());
		builder.append(", longitude=");
		builder.append(coordinate.getLongitude());
		builder.append("}");
		return builder.toString();
	}
	
	@Test
	public void shouldBeCheckCreatedCoordinateWhenGivenAttribetesCanBeControlled() {
		Coordinate coordinate = new Coordinate();
		int LATITUDE = 10;
		int LONGITUDE = 20;
		coordinate.setLatitude(LATITUDE);
		coordinate.setLongitude(LONGITUDE);
		
		Assert.assertEquals(LATITUDE, ZERO, coordinate.getLatitude());
		Assert.assertEquals(LONGITUDE, ZERO, coordinate.getLongitude());
	}
	
}
