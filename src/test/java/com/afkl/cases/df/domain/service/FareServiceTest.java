package com.afkl.cases.df.domain.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.afkl.cases.df.domain.FareInfo;
import com.afkl.cases.df.domain.dto.FareInfoResultDto;
import com.afkl.cases.df.service.FareService;

@TestPropertySource(locations = "classpath:application-test.properties")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class FareServiceTest {

	@Value("${api.endpoint.base}")
	private String baseUrl;

	@Value("${api.endpoint.fare}")
	private String fareUrl;

	@Value("${api.endpoint.timeout}")
	private long timeout;

	@Value("${api.endpoint.authUrl}")
	private String authUrl;

	@Value("${api.endpoint.clientId}")
	private String clientId;

	@Value("${api.endpoint.clientSecret}")
	private String secret;

	@Value("${api.endpoint.grantType}")
	private String grantType;

	@InjectMocks
	private FareService fareService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		fareService.generateRestTemplate(generateRestTemplate());
		fareService.generatedValueFromApplicationProperties(baseUrl, fareUrl, timeout);
	}

	private OAuth2RestTemplate generateRestTemplate() {
		ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
		resourceDetails.setAccessTokenUri(authUrl);
		resourceDetails.setClientId(clientId);
		resourceDetails.setClientSecret(secret);
		resourceDetails.setGrantType(grantType);
		resourceDetails.setScope(Arrays.asList("read", "write"));

		return new OAuth2RestTemplate(resourceDetails, new DefaultOAuth2ClientContext());
	}

	@Test
	public void shouldBeTestFareServiceWhenGivenTextIsNull() throws IOException {
		Future<FareInfoResultDto> fareInfoResultDto = fareService.findFare(null, null);
		Assert.assertNull(fareInfoResultDto);
	}

	@Test
	public void shouldBeTestFareServiceWhenGivenTextIsEmpty() throws IOException {
		Future<FareInfoResultDto> fareInfoResultDto = fareService.findFare(StringUtils.EMPTY, StringUtils.EMPTY);
		Assert.assertNull(fareInfoResultDto);
	}

	@Test
	public void shouldBeTestFareServiceWhenGivenTextIsNotEmpty() throws IOException, InterruptedException, ExecutionException {
		Future<FareInfoResultDto> fareInfoResultDto = fareService.findFare("YQM", "SEA");
		Assert.assertNotNull(fareInfoResultDto);
		
		Assert.assertNotNull(fareInfoResultDto.get());
	}

	@Test
	public void shouldBeTestFareServiceGenerateFareWhenGivenTextIsNotEmpty() throws IOException, InterruptedException, ExecutionException {
		Future<FareInfoResultDto> fareInfoResultDto = fareService.findFare("YQM", "SEA");
		Assert.assertNotNull(fareInfoResultDto);
		
		FareInfo fareInfo = fareService.genarateFareInfo(fareInfoResultDto.get(), null, null);
		Assert.assertNotNull(fareInfo);
	}

}
