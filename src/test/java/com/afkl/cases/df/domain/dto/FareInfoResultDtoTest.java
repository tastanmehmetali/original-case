package com.afkl.cases.df.domain.dto;

import org.junit.Assert;
import org.junit.Test;

import com.afkl.cases.df.domain.util.Currency;

public class FareInfoResultDtoTest {

	@Test
	public void shouldBeTestFareInfoResultDtoWhenItIsNull() {
		FareInfoResultDto fareInfoResultDto = null;
		Assert.assertNull(fareInfoResultDto);
	}
	
	@Test
	public void shouldBeTestFareInfoResultDtoWhenItIsCreated() {
		FareInfoResultDto fareInfoResultDto = new FareInfoResultDto();
		Assert.assertNotNull(fareInfoResultDto);
	}
	
	@Test
	public void shouldBeTestFareInfoResultDtoWhenAttributesCanBeChecked() {
		FareInfoResultDto fareInfoResultDto = new FareInfoResultDto();
		fareInfoResultDto.setAmount(10);
		fareInfoResultDto.setCurrency(Currency.EUR);
		fareInfoResultDto.setDestination("AMS");
		fareInfoResultDto.setOrigin("JFK");
		
		Assert.assertEquals(10, 0, fareInfoResultDto.getAmount());
		Assert.assertEquals(Currency.EUR, fareInfoResultDto.getCurrency());
		Assert.assertEquals("AMS", fareInfoResultDto.getDestination());
		Assert.assertEquals("JFK", fareInfoResultDto.getOrigin());
	}

	@Test
	public void shouldBeTestFareInfoResultDtoToString() {
		FareInfoResultDto fareInfoResultDto = new FareInfoResultDto();
		Assert.assertEquals(generatedToString(fareInfoResultDto), fareInfoResultDto.toString());
	}
	
	private String generatedToString(FareInfoResultDto fareInfoResultDto) {
		StringBuilder builder = new StringBuilder();
		builder.append("FareInfoResultDto{ amount='");
		builder.append(fareInfoResultDto.getAmount());
		builder.append(", currency=");
		builder.append(fareInfoResultDto.getCurrency());
		builder.append(", origin=");
		builder.append(fareInfoResultDto.getOrigin());
		builder.append(", destination=");
		builder.append(fareInfoResultDto.getDestination());
		builder.append("}");
		return builder.toString();
	}
}
