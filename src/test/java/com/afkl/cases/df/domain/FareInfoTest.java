package com.afkl.cases.df.domain;

import org.junit.Assert;
import org.junit.Test;

import com.afkl.cases.df.domain.util.Currency;

public class FareInfoTest {

	private static final int ZERO = 0;

	@Test
	public void shouldBeTestFareInfoWhenItIsNull() {
		FareInfo fareInfo = null;
		Assert.assertNull(fareInfo);
	}
	
	@Test
	public void shouldBeTestFareInfoWhenItIsNewObject() {
		FareInfo fareInfo = new FareInfo();
		Assert.assertNotNull(fareInfo);
	}
	
	@Test
	public void shouldBeTestFareInfoWhenCreatedFareHasDefaultCurrency() {
		FareInfo fareInfo = new FareInfo();
		Assert.assertEquals(Currency.EUR, fareInfo.getCurrency());
	}
	
	@Test
	public void shouldBeTestFareInfoWhenCurrencyIsChangedUSD() {
		FareInfo fareInfo = new FareInfo();
		fareInfo.setCurrency(Currency.USD);
		Assert.assertEquals(Currency.USD, fareInfo.getCurrency());
	}
	
	@Test
	public void shouldBeTestFareInfoWhenCurrencyIsNullSet() {
		FareInfo fareInfo = new FareInfo();
		fareInfo.setCurrency(null);
		Assert.assertEquals(Currency.EUR, fareInfo.getCurrency());
	}
	
	@Test
	public void shouldBeTestFareInfoWhenGivenAttribetesCanBeControlled() {
		FareInfo fareInfo = new FareInfo();
		double givenAmmount = 100;
		String givenDestination = "AMS";
		String givenOrgin = "JFK";
		fareInfo.setAmount(givenAmmount);
		
		Location destLocation = generateLocationForGivenCode(givenDestination);
		fareInfo.setDestination(destLocation);
		
		Location orgLocation = generateLocationForGivenCode(givenOrgin);
		fareInfo.setOrgin(orgLocation);
		
		Assert.assertEquals(Currency.EUR, fareInfo.getCurrency());
		Assert.assertEquals(givenAmmount, ZERO, fareInfo.getAmount());
		
		Assert.assertNotNull(fareInfo.getDestination());
		Assert.assertEquals(givenDestination, fareInfo.getDestination().getCode());

		Assert.assertNotNull(fareInfo.getOrgin());
		Assert.assertEquals(givenOrgin, fareInfo.getOrgin().getCode());
	}
	
	@Test
	public void shouldBeTestFareInfoToString() {
		FareInfo fareInfo = new FareInfo();
		double givenAmmount = 100;
		String givenDestination = "AMS";
		String givenOrgin = "JFK";
		fareInfo.setAmount(givenAmmount);
		
		Location destLocation = generateLocationForGivenCode(givenDestination);
		fareInfo.setDestination(destLocation);
		
		Location orgLocation = generateLocationForGivenCode(givenOrgin);
		fareInfo.setOrgin(orgLocation);
		
		Assert.assertEquals(generateToString(fareInfo), fareInfo.toString());
	}
	
	private String generateToString(FareInfo fareInfo) {
		StringBuilder builder = new StringBuilder();
		builder.append("FareInfo { amount=");
		builder.append(fareInfo.getAmount());
		builder.append(", currency=");
		builder.append(fareInfo.getCurrency());
		builder.append(", orgin=");
		builder.append(fareInfo.getOrgin());
		builder.append(", destination=");
		builder.append(fareInfo.getDestination());
		builder.append("}");
		return builder.toString();
	}

	private Location generateLocationForGivenCode(String givenCode) {
		Location location = new Location();
		location.setCode(givenCode);
		return location;
	}
	
}
