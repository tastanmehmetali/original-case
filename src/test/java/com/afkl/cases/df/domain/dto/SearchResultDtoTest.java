package com.afkl.cases.df.domain.dto;

import org.junit.Assert;
import org.junit.Test;

public class SearchResultDtoTest {

	@Test
	public void shouldBeTestSearchResultDtoWhenItIsNull() {
		SearchResultDto searchResultDto = null;
		Assert.assertNull(searchResultDto);
	}

	@Test
	public void shouldBeTestSearchResultDtoWhenItIsCreated() {
		SearchResultDto searchResultDto = new SearchResultDto();
		Assert.assertNotNull(searchResultDto);
	}

	@Test
	public void houldBeTestSearchResultDtoToString() {
		SearchResultDto searchResultDto = new SearchResultDto();
		Assert.assertEquals(generatedToString(searchResultDto), searchResultDto.toString());
	}
	
	private String generatedToString(SearchResultDto searchResultDto) {
		StringBuilder builder = new StringBuilder();
		builder.append("SearchResultDto{ _embedded='");
		builder.append(searchResultDto.getLocationResultDto());
		builder.append("}");
		return builder.toString();
	}

	@Test
	public void shouldBeTestSearchResultDtoWhenItHasAttributes() {
		SearchResultDto searchResultDto = new SearchResultDto();
		LocationResultDto locationResultDto = new LocationResultDto();
		searchResultDto.setLocationResultDto(locationResultDto);
		Assert.assertEquals(locationResultDto, searchResultDto.getLocationResultDto());
	}

}
