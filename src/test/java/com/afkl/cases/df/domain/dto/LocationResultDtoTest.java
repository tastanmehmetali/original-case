package com.afkl.cases.df.domain.dto;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.afkl.cases.df.domain.Location;

public class LocationResultDtoTest {

	@Test
	public void shouldBeTestLocationResultDtoWhenItIsNull() {
		LocationResultDto locationResultDto = null;
		Assert.assertNull(locationResultDto);
	}
	
	@Test
	public void shouldBeTestLocationResultDtoWhenItIsCreated() {
		LocationResultDto locationResultDto = new LocationResultDto();
		Assert.assertNotNull(locationResultDto);
	}

	@Test
	public void shouldBeTestLocationResultDtoToString() {
		LocationResultDto locationResultDto = new LocationResultDto();
		Assert.assertEquals(generatedToString(locationResultDto), locationResultDto.toString());
	}
	
	private String generatedToString(LocationResultDto locationResultDto) {
		StringBuilder builder = new StringBuilder();
		builder.append("LocationResultDto{ locations='");
		builder.append(locationResultDto.getLocations());
		builder.append("}");
		return builder.toString();
	}
	
	@Test
	public void shouldBeTestLocationResultDtoWhenItHasLocations() {
		LocationResultDto locationResultDto = new LocationResultDto();
		Location firstLocation = new Location();
		Location secondLocation = new Location();
		List<Location> locations = new ArrayList<>();
		locations.add(firstLocation);
		locations.add(secondLocation);
		locationResultDto.setLocations(locations);
		
 		Assert.assertEquals(locations.size(), locationResultDto.getLocations().size());
 		Assert.assertEquals(locations, locationResultDto.getLocations());
	}
}
