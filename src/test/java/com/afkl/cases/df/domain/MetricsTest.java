package com.afkl.cases.df.domain;

import org.junit.Assert;
import org.junit.Test;

public class MetricsTest {

	private final static int ZERO = 0;
	private final static int ONE = 1;
	private final static long TEN = 10;
	
	@Test
	public void shouldBeCheckMetricsWhenItIsNull() {
		Metrics metrics = null;
		Assert.assertNull(metrics);
	}

	@Test
	public void shouldBeCheckMetricsWhenItIsNewObject() {
		Metrics metrics = new Metrics();
		Assert.assertNotNull(metrics);
	}

	@Test
	public void shouldBeCheckMetricsWhenItHasCreatedAttributes() {
		Metrics metrics = new Metrics();
		Assert.assertNotNull(metrics);
		Assert.assertEquals(ZERO, metrics.getRequestCount());
		Assert.assertEquals(ZERO, metrics.getRequestCountOK());
		Assert.assertEquals(ZERO, metrics.getRequestCount4xx());
		Assert.assertEquals(ZERO, metrics.getRequestCount5xx());
		Assert.assertEquals(ZERO, metrics.getAvgResponseTime());
		Assert.assertEquals(ZERO, metrics.getMinResponseTime());
		Assert.assertEquals(ZERO, metrics.getMaxResponseTime());
	}

	@Test
	public void shouldBeCheckMetricsSetValueWhenItHasCreatedAttributes() {
		Metrics metrics = new Metrics();
		
		metrics.setAvgResponseTime(TEN);
		metrics.setMinResponseTime(TEN);
		metrics.setMaxResponseTime(TEN);

		metrics.setRequestCount(ONE);
		metrics.setRequestCountOK(ONE);
		metrics.setRequestCount4xx(ONE);
		metrics.setRequestCount5xx(ONE);
		
		Assert.assertNotNull(metrics);
		Assert.assertEquals(ONE, metrics.getRequestCount());
		Assert.assertEquals(ONE, metrics.getRequestCountOK());
		Assert.assertEquals(ONE, metrics.getRequestCount4xx());
		Assert.assertEquals(ONE, metrics.getRequestCount5xx());
		Assert.assertEquals(TEN, metrics.getAvgResponseTime());
		Assert.assertEquals(TEN, metrics.getMinResponseTime());
		Assert.assertEquals(TEN, metrics.getMaxResponseTime());
	}

}
