package com.afkl.cases.df.domain.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import com.afkl.cases.df.domain.Location;
import com.afkl.cases.df.domain.dto.LocationResultDto;
import com.afkl.cases.df.service.AirportService;

@TestPropertySource(locations="classpath:application-test.properties")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class AirportServiceTest {

	private static final String AIRPORT_CODE_AMS = "AMS";

	@Value("${api.endpoint.base}")
	private String baseUrl;

	@Value("${api.endpoint.airport}")
	private String airportUrl;

	@Value("${api.endpoint.airport.search}")
	private String airportUrlForSearch;

    @Value("${api.endpoint.authUrl}")
    private String authUrl;

    @Value("${api.endpoint.clientId}")
    private String clientId;

    @Value("${api.endpoint.clientSecret}")
    private String secret;
    
    @Value("${api.endpoint.grantType}")
    private String grantType;
	
	@InjectMocks
	private AirportService airportService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		airportService.generateRestTemplate(generateRestTemplate());
		airportService.generatedValueFromApplicationProperties(baseUrl, airportUrl, airportUrlForSearch);
	}

    private OAuth2RestTemplate generateRestTemplate() {
        ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
        resourceDetails.setAccessTokenUri(authUrl);
        resourceDetails.setClientId(clientId);
        resourceDetails.setClientSecret(secret);
        resourceDetails.setGrantType(grantType);
        resourceDetails.setScope(Arrays.asList("read", "write"));

        return new OAuth2RestTemplate(resourceDetails, new DefaultOAuth2ClientContext());
    }
	
	@Test
	public void shouldBeTestAirportServiceWhenGivenTextIsNull() throws IOException {
		Future<LocationResultDto> locationResult = airportService.getAirports(null);
		Assert.assertNotNull(locationResult);
	}
	
	@Test
	public void shouldBeTestAirportServiceWhenGivenTextIsEmpty() throws IOException {
		Future<LocationResultDto> locationResult = airportService.getAirports(StringUtils.EMPTY);
		Assert.assertNotNull(locationResult);
	}
	
	@Test
	public void shouldBeTestAirportServiceWhenGivenTextIsNotEmpty() throws IOException {
		Future<LocationResultDto> locationResult = airportService.getAirports(AIRPORT_CODE_AMS);
		Assert.assertNotNull(locationResult);
	}
	
	@Test
	public void shouldBeTestAirportServiceWhenGivenTextIsAMS() throws IOException, InterruptedException, ExecutionException {
		Future<LocationResultDto> locationResult = airportService.getAirports(AIRPORT_CODE_AMS);
		Assert.assertNotNull(locationResult);
		
		LocationResultDto locationResultDto = locationResult.get();
		Assert.assertNotNull(locationResultDto);
		
		Assert.assertNotNull(locationResultDto.getLocations());
		Assert.assertFalse(locationResultDto.getLocations().isEmpty());
	}

	@Test
	public void shouldBeTestAirportServiceFindAirportCodeWhenGivenTextIsNull() throws IOException {
		Future<Location> locationFuture = airportService.findAirportByGivenCode(null);
		Assert.assertNull(locationFuture);
	}

	@Test
	public void shouldBeTestAirportServiceFindAirportCodeWhenGivenTextIsEmpty() throws IOException {
		Future<Location> locationFuture = airportService.findAirportByGivenCode(StringUtils.EMPTY);
		Assert.assertNull(locationFuture);
	}

	@Test
	public void shouldBeTestAirportServiceFindAirportCodeWhenGivenTextIsNotEmpty() throws IOException {
		Future<Location> locationFuture = airportService.findAirportByGivenCode(AIRPORT_CODE_AMS);
		Assert.assertNotNull(locationFuture);
	}

	@Test(expected = HttpClientErrorException.class)
	public void shouldBeTestAirportServiceFindAirportCodeWhenGivenTextIsNotEmptyNotExist() throws IOException {
		Future<Location> locationFuture = airportService.findAirportByGivenCode("asddsa");
		Assert.assertNotNull(locationFuture);
	}
}
