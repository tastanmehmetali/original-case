package com.afkl.cases.df.domain;

import org.junit.Assert;
import org.junit.Test;

public class LocationTest {

	private static final String LOCATION_NAME = "Amsterdam Schiphol Airport";
	private static final String LOCATION_AIRPORT = "Airport";
	private static final String LOCATION_CODE = "AMS";
	private static final double ZERO = 0;

	@Test
	public void shouldBeCheckLocationWhenItIsNull() {
		Location location = null;
		Assert.assertNull(location);
	}

	@Test
	public void shouldBeCheckLocationWhenItIsNewObject() {
		Location location = new Location();
		Assert.assertNotNull(location);
	}

	@Test
	public void shouldBeCheckLocationtoString() {
		Location location = new Location();
		Assert.assertEquals(generateToString(location), location.toString());
	}

	private String generateToString(Location location) {
		StringBuilder builder = new StringBuilder();
		builder.append("Location{ code='");
		builder.append(location.getCode());
		builder.append("', name='");
		builder.append(location.getName());
		builder.append("', description='");
		builder.append(location.getDescription());
		builder.append("', parent=");
		builder.append(location.getParent());
		builder.append("', coordinate=");
		builder.append(location.getCoordinates());
		builder.append("}");
		return builder.toString();
	}

	@Test
	public void shouldBeCheckLocationWhenGivenAttribetesCanBeControlled() {
		Location location = new Location();
		location.setCode(LOCATION_CODE);
		location.setDescription(LOCATION_AIRPORT);
		location.setName(LOCATION_NAME);
		
		Assert.assertEquals(LOCATION_CODE, location.getCode());
		Assert.assertEquals(LOCATION_AIRPORT, location.getDescription());
		Assert.assertEquals(LOCATION_NAME, location.getName());
		
		Assert.assertNull(location.getParent());
	}
	
	@Test
	public void shouldBeCheckLocationWhenItHasParentLocation() {
		Location location = new Location();
		Location parentLocation = new Location();
		parentLocation.setCode(LOCATION_CODE);
		parentLocation.setDescription(LOCATION_AIRPORT);
		parentLocation.setName(LOCATION_NAME);
		
		location.setParent(parentLocation);
		Assert.assertNotNull(location.getParent());
		Assert.assertEquals(LOCATION_CODE, location.getParent().getCode());
		Assert.assertEquals(LOCATION_AIRPORT, location.getParent().getDescription());
		Assert.assertEquals(LOCATION_NAME, location.getParent().getName());
	}
	
	@Test
	public void shouldBeCheckLocationWhenItHasCoordinates() {
		Location location = new Location();
		Coordinate coordinate = new Coordinate();
		int LATITUDE = 10;
		int LONGITUDE = 20;
		coordinate.setLatitude(LATITUDE);
		coordinate.setLongitude(LONGITUDE);
		
		location.setCoordinates(coordinate);
		
		Assert.assertNotNull(location.getCoordinates());
		Assert.assertEquals(LATITUDE, ZERO, location.getCoordinates().getLatitude());
		Assert.assertEquals(LONGITUDE, ZERO, location.getCoordinates().getLongitude());
		
		
	}
	
}
